VENDOR_PATH := vendor/ANXCamera

PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/etc,system/etc) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm,system/priv-app/ANXCamera/lib/arm) \
   	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64,system/priv-app/ANXCamera/lib/arm64) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXExtraPhoto/lib/arm64,system/priv-app/ANXExtraPhoto/lib/arm64) \
        $(call find-copy-subdir-files,*,$(VENDOR_PATH)/patches/bin/,system/bin) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/patches/lib/,system/lib) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/patches/lib64/,system/lib64)

PRODUCT_PACKAGES += \
	anxfilecheck.sh \
	anxcamera.rc

PRODUCT_PACKAGES += \
	ANXCamera \
	ANXExtraPhoto
